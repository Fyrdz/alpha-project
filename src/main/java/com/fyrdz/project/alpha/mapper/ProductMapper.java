package com.fyrdz.project.alpha.mapper;

import com.fyrdz.project.alpha.dto.response.LiveProductDetailsResponseDto;
import com.fyrdz.project.alpha.dto.response.ProductDetailsResponseDto;
import com.fyrdz.project.alpha.repository.entity.ProductEntity;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory mapperFactory) {
        mapperFactory.classMap(ProductEntity.class, ProductDetailsResponseDto.class)
                .byDefault()
                .mapNulls(false)
                .register();
        mapperFactory.classMap(ProductEntity.class, LiveProductDetailsResponseDto.class)
                .byDefault()
                .mapNulls(false)
                .register();
    }
}
