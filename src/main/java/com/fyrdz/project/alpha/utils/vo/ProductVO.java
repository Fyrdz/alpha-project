package com.fyrdz.project.alpha.utils.vo;

public interface ProductVO {
     String getId();
     String getName();
     String getBrandName();
     String getProductType();
     int getQuantity();
     Long getRowNo();
}
