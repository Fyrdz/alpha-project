package com.fyrdz.project.alpha.utils;

import com.fyrdz.project.alpha.dto.request.PagingValueRequestDto;
import com.fyrdz.project.alpha.dto.response.CursorDto;
import com.fyrdz.project.alpha.dto.response.PagingResponseDto;
import com.fyrdz.project.alpha.utils.vo.ProductVO;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log4j2
public class Util {

    public PagingResponseDto getPage(List<ProductVO> ProductVOList, PagingValueRequestDto request) {
        CursorDto cursor = new CursorDto();
        PagingResponseDto paging = new PagingResponseDto();
        if (request.getPrevious().equals(0) && request.getNext().equals(0)) {
            if (ProductVOList.isEmpty()) {
                cursor.setBefore(0);
                cursor.setAfter(0);
                paging.setCursors(cursor);
                paging.setNext(0);
            } else {
                ProductVO lastValue = ProductVOList.get(ProductVOList.size() - 1);
                cursor.setBefore(0);
                cursor.setAfter(lastValue.getRowNo().intValue());
                paging.setCursors(cursor);
                paging.setNext(lastValue.getRowNo().intValue() + 1);
            }
        } else {
            if (ProductVOList.isEmpty()) {
                cursor.setBefore(request.getPrevious());
                cursor.setAfter(0);
                paging.setCursors(cursor);
                paging.setNext(0);
            } else {
                ProductVO lastValue = ProductVOList.get(ProductVOList.size() - 1);
                cursor.setBefore(request.getPrevious());
                cursor.setAfter(lastValue.getRowNo().intValue());
                paging.setCursors(cursor);
                paging.setNext(lastValue.getRowNo().intValue() + 1);
            }
        }
        return paging;
    }
}
