package com.fyrdz.project.alpha.config.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class KafkaCommonProperties {

    @Value("${props.kafka.consumer.concurrency}")
    private String listenerConcurrency;
}
