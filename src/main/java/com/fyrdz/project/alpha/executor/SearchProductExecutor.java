package com.fyrdz.project.alpha.executor;

import com.fyrdz.project.alpha.dto.request.SearchProductRequestDto;
import com.fyrdz.project.alpha.dto.response.PagingResponseDto;
import com.fyrdz.project.alpha.dto.response.ProductResponseDto;
import com.fyrdz.project.alpha.dto.response.SearchProductResponseDto;
import com.fyrdz.project.alpha.repository.service.ProductEntityRepositoryService;
import com.fyrdz.project.alpha.utils.Util;
import com.fyrdz.project.alpha.utils.vo.ProductVO;
import lombok.extern.log4j.Log4j2;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Component
public class SearchProductExecutor {

    @Autowired
    private ProductEntityRepositoryService productService;

    @Autowired
    private MapperFacade mapperFacade;

    @Autowired
    private Util util;

    public ResponseEntity<SearchProductResponseDto> searchProduct(HttpServletRequest httpRequest, SearchProductRequestDto request) {
        List<ProductVO> productVOList;
        String name = request.getName() != null ? "%" + request.getName().toUpperCase() + "%" : "%";
        if (request.getPaging().getPrevious().equals(0) && request.getPaging().getNext().equals(0)) {
            productVOList = productService.findProductListInFirstPage(name, request.getPaging().getSize());
        } else {
            productVOList = productService.findProductListInNextPage(name, request.getPaging().getNext(),
                    request.getPaging().getPrevious() + request.getPaging().getSize());
        }
        List<ProductResponseDto> productList = productVOList.stream()
                .map(productVO -> {
                    ProductResponseDto product = new ProductResponseDto();
                    product.setId(productVO.getId());
                    product.setName(productVO.getName());
                    product.setBrandName(productVO.getBrandName());
                    product.setProductType(productVO.getProductType());
                    product.setQuantity(productVO.getQuantity());
                    return product;
                })
                .collect(Collectors.toList());
        SearchProductResponseDto response = new SearchProductResponseDto();
        PagingResponseDto pagingResponse = util.getPage(productVOList, request.getPaging());
        response.setProducts(productList);
        response.setPaging(pagingResponse);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}

