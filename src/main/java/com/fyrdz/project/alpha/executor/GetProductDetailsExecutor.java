package com.fyrdz.project.alpha.executor;

import com.fyrdz.project.alpha.dto.response.ProductDetailsResponseDto;
import com.fyrdz.project.alpha.repository.entity.ProductEntity;
import com.fyrdz.project.alpha.repository.service.ProductEntityRepositoryService;
import lombok.extern.log4j.Log4j2;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Log4j2
@Component
public class GetProductDetailsExecutor {

    @Autowired
    private ProductEntityRepositoryService productService;

    @Autowired
    private MapperFacade mapperFacade;

    public ResponseEntity<ProductDetailsResponseDto> getProductDetails(HttpServletRequest httpRequest, String id) {
        Optional<ProductEntity> productOptional = productService.findById(id);

        ProductDetailsResponseDto response = productOptional.map(this::mapEntityToDto)
                .orElse(new ProductDetailsResponseDto());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    private ProductDetailsResponseDto mapEntityToDto(ProductEntity product) {
        return mapperFacade.map(product, ProductDetailsResponseDto.class);
    }
}
