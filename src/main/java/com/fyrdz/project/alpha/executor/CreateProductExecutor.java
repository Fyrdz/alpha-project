package com.fyrdz.project.alpha.executor;

import com.fyrdz.project.alpha.dto.request.ModifyProductRequestDto;
import com.fyrdz.project.alpha.dto.response.UpdateProductResponseDto;
import com.fyrdz.project.alpha.repository.entity.ProductEntity;
import com.fyrdz.project.alpha.repository.service.ProductEntityRepositoryService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@Component
public class CreateProductExecutor {

    @Autowired
    private ProductEntityRepositoryService productService;

    public ResponseEntity<UpdateProductResponseDto> createProduct(HttpServletRequest httpRequest, ModifyProductRequestDto request) {
        ProductEntity product = new ProductEntity();
        product.setName(request.getName());
        product.setBrandName(request.getBrandName());
        product.setProductType(request.getProductType());
        product.setOrderType(request.getOrderType());
        product.setLive(request.isLive());
        product.setQuantity(request.getQuantity());
        productService.save(product);

        UpdateProductResponseDto response = new UpdateProductResponseDto();
        response.setStatus("SUCCESS");
        response.setMessage("Success creating product");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}
