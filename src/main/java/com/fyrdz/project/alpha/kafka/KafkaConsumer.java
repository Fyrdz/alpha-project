package com.fyrdz.project.alpha.kafka;


import brave.Tracing;
import brave.propagation.TraceContext;
import com.fyrdz.avro.schema.Order;
import com.fyrdz.avro.schema.UpdateStock;
import com.fyrdz.project.alpha.repository.service.ProductEntityRepositoryService;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
public class KafkaConsumer {

    @Autowired
    private Tracing tracing;

    @Autowired
    private ProductEntityRepositoryService productEntityRepositoryService;

    @KafkaListener(topics = "${props.kafka.consumer.topic}", containerFactory = "kafkaListenerContainerFactory")
    public void consumeUpdateStock(ConsumerRecord<String, UpdateStock> records) throws Exception {
        TraceContext.Injector<Headers> injector = tracing.propagation()
                .injector((headers, s, s2) -> headers.add(new RecordHeader(s, s2.getBytes())));
        injector.inject(tracing.currentTraceContext().get(), records.headers());
        log.info("CONSUMER START");

        UpdateStock updateStock = records.value();

        for (Order order : updateStock.getOrderList()) {
            productEntityRepositoryService.findById(order.getId())
                    .ifPresent(product -> {
                        product.setQuantity(product.getQuantity() - order.getQuantity());
                        productEntityRepositoryService.save(product);
                    });
        }

        log.info("CONSUMER END");
    }

}
