package com.fyrdz.project.alpha.controller;

import com.fyrdz.project.alpha.dto.request.SearchProductRequestDto;
import com.fyrdz.project.alpha.dto.response.LiveProductDetailsResponseDto;
import com.fyrdz.project.alpha.dto.response.SearchProductResponseDto;
import com.fyrdz.project.alpha.executor.GetLiveProductDetailsExecutor;
import com.fyrdz.project.alpha.executor.SearchActiveProductExecutor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/v1/market")
@Log4j2
public class ProductSellingController {

    @Autowired
    private SearchActiveProductExecutor searchActiveProductExecutor;

    @Autowired
    private GetLiveProductDetailsExecutor getLiveProductDetailsExecutor;

    @PostMapping("/search")
    public ResponseEntity<SearchProductResponseDto> searchActiveProduct(HttpServletRequest httpRequest,
                                                                        @RequestBody SearchProductRequestDto request) {
        return searchActiveProductExecutor.searchProduct(httpRequest, request);
    }

    @PostMapping("/search/{id}")
    public ResponseEntity<LiveProductDetailsResponseDto> getProductDetails(HttpServletRequest httpRequest,
                                                                           @PathVariable String id) {
        return getLiveProductDetailsExecutor.getProductDetails(httpRequest, id);
    }
}
