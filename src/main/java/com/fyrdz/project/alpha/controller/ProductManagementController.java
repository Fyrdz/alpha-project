package com.fyrdz.project.alpha.controller;

import com.fyrdz.project.alpha.dto.request.ModifyProductRequestDto;
import com.fyrdz.project.alpha.dto.request.SearchProductRequestDto;
import com.fyrdz.project.alpha.dto.response.ProductDetailsResponseDto;
import com.fyrdz.project.alpha.dto.response.SearchProductResponseDto;
import com.fyrdz.project.alpha.dto.response.UpdateProductResponseDto;
import com.fyrdz.project.alpha.executor.CreateProductExecutor;
import com.fyrdz.project.alpha.executor.GetProductDetailsExecutor;
import com.fyrdz.project.alpha.executor.SearchProductExecutor;
import com.fyrdz.project.alpha.executor.UpdateProductExecutor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/v1/product")
@Log4j2
public class ProductManagementController {

    @Autowired
    private CreateProductExecutor createProductExecutor;

    @Autowired
    private UpdateProductExecutor updateProductExecutor;

    @Autowired
    private GetProductDetailsExecutor getProductDetailsExecutor;

    @Autowired
    private SearchProductExecutor searchProductExecutor;

    @PostMapping("/create")
    public ResponseEntity<UpdateProductResponseDto> createProduct(HttpServletRequest httpRequest,
                                                                  @RequestBody ModifyProductRequestDto request) {
        return createProductExecutor.createProduct(httpRequest, request);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<UpdateProductResponseDto> updateProduct(HttpServletRequest httpRequest,
                                                                  @PathVariable("id") String id,
                                                                  @RequestBody ModifyProductRequestDto request) {
        return updateProductExecutor.updateProduct(httpRequest, id, request);
    }

    @GetMapping("/search")
    public ResponseEntity<SearchProductResponseDto> searchProduct(HttpServletRequest httpRequest,
                                                                  @RequestBody SearchProductRequestDto request) {
        return searchProductExecutor.searchProduct(httpRequest, request);
    }

    @GetMapping("/search/{id}")
    public ResponseEntity<ProductDetailsResponseDto> getProductDetails(HttpServletRequest httpRequest, @PathVariable String id) {
        return getProductDetailsExecutor.getProductDetails(httpRequest, id);
    }
}
