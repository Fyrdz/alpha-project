package com.fyrdz.project.alpha.repository.service;

import com.fyrdz.project.alpha.repository.entity.ProductEntity;
import com.fyrdz.project.alpha.utils.vo.ProductVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductEntityRepositoryService extends JpaRepository<ProductEntity, String> {
    @Query(value = "select pe.id as id, pe.name as name, pe.brand_name as brandName, pe.product_type as productType," +
            "pe.quantity as quantity, row_number() over (order by upper(pe.name)) " +
            "as rowNo from product pe where upper(pe.name) like upper(:name) fetch first :size rows only", nativeQuery = true)
    List<ProductVO> findProductListInFirstPage(@Param("name") String name,
                                               @Param("size") int size);

    @Query(value = "with q as(select id, name, brandName, productType, rowNo " +
            "from (select pe.id as id, pe.name as name, pe.brand_name as brandName, pe.product_type as productType, " +
            "pe.quantity as quantity, row_number() over (order by upper(pe.name)) as rowNo " +
            "from product pe " +
            "where upper(pe.name) like upper(:name))) " +
            "select * from q where rowNo BETWEEN :bottom AND :top " +
            "order BY rowNo", nativeQuery = true)
    List<ProductVO> findProductListInNextPage(@Param("name") @Nullable String name,
                                              @Param("bottom") int bottom,
                                              @Param("top") int top);

    @Query(value = "select pe.id as id, pe.name as name, pe.brand_name as brandName, pe.product_type as productType," +
            "pe.quantity as quantity, row_number() over (order by upper(pe.name)) " +
            "as rowNo from product pe where upper(pe.name) like upper(:name) and pe.is_live is true " +
            "fetch first :size rows only", nativeQuery = true)
    List<ProductVO> findLiveProductListInFirstPage(@Param("name") String name,
                                                   @Param("size") int size);

    @Query(value = "with q as(select id, name, brandName, productType, quantity, rowNo " +
            "from (select pe.id as id, pe.name as name, pe.brand_name as brandName, pe.product_type as productType, " +
            "pe.quantity as quantity, row_number() over (order by upper(pe.name)) as rowNo " +
            "from product pe " +
            "where upper(pe.name) like upper(:name) and pe.is_live IS true)) " +
            "select * from q where rowNo BETWEEN :bottom AND :top " +
            "order BY rowNo", nativeQuery = true)
    List<ProductVO> findLiveProductListInNextPage(@Param("name") @Nullable String name,
                                                  @Param("bottom") int bottom,
                                                  @Param("top") int top);

}