package com.fyrdz.project.alpha.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class LiveProductDetailsResponseDto {
    private String id;
    private String name;
    private String brandName;
    private String productType;
    private String orderType;
    private Integer quantity;
}
