package com.fyrdz.project.alpha.dto.response;

import lombok.Data;

@Data
public class PagingResponseDto {
    private CursorDto cursors;
    private Integer next;
}
