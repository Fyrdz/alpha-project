package com.fyrdz.project.alpha.dto.request;

import lombok.Data;

@Data
public class ModifyProductRequestDto {
    private String name;
    private String brandName;
    private String productType;
    private String orderType;
    private boolean isLive;
    private int quantity;
}
