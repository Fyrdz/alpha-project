package com.fyrdz.project.alpha.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class SearchProductResponseDto {
    private List<ProductResponseDto> products;
    private PagingResponseDto paging;
}
