package com.fyrdz.project.alpha.dto.response;

import lombok.Data;

@Data
public class ProductResponseDto {
    private String id;
    private String name;
    private String brandName;
    private String productType;
    private int quantity;
}
