package com.fyrdz.project.alpha.dto.request;

import lombok.Data;
import lombok.NonNull;

@Data
public class PagingValueRequestDto {
    private Integer previous;
    private Integer next;
    private Integer size;
}
