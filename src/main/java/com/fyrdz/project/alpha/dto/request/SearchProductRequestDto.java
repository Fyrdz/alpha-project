package com.fyrdz.project.alpha.dto.request;

import lombok.Data;

@Data
public class SearchProductRequestDto {
    private String name;
    private PagingValueRequestDto paging;
}
