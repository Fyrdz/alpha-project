package com.fyrdz.project.alpha.dto.response;

import lombok.Data;

@Data
public class CursorDto {
    private Integer before;
    private Integer after;
}
