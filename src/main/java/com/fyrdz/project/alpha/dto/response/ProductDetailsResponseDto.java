package com.fyrdz.project.alpha.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class ProductDetailsResponseDto {
    private String id;
    private String name;
    private String brandName;
    private String productType;
    private String orderType;
    private boolean isLive;
    private Integer quantity;
    private Date createdDate;
    private Date updatedDate;
}
