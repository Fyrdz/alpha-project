package com.fyrdz.project.alpha.dto.response;

import lombok.Data;

@Data
public class UpdateProductResponseDto {
    private String status;
    private String message;
}
