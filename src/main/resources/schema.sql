CREATE TABLE IF NOT EXISTS product
(
    id varchar(100) NOT NULL,
    name varchar(100) NOT NULL,
    brand_name varchar(100) NOT NULL,
    product_type varchar(100) NOT NULL,
    order_type varchar(100) NOT NULL,
    is_live bool NOT NULL,
    quantity bigint DEFAULT 0,
    created_date timestamp,
    updated_date timestamp,
    CONSTRAINT product_pkey PRIMARY KEY (id)
);