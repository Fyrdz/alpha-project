--Data Init
INSERT INTO product
(id, brand_name, created_date, is_live, "name", order_type, product_type, quantity, updated_date)
VALUES('c792da12-1022-4b9a-b00e-73d3e9dd3131', 'Qwertykeys', '2024-01-14', true, 'QK75', 'Group Buy', 'Mechanical Keyboard', 10, '2024-01-14'),
('c792da12-1022-4b9a-b00e-73d3e9dd3132', 'Qwertykeys', '2024-01-14', true, 'QK80', 'Group Buy', 'Mechanical Keyboard', 10, '2024-01-14'),
('c792da12-1022-4b9a-b00e-73d3e9dd3133', 'Qwertykeys', '2024-01-14', true, 'QK60', 'Group Buy', 'Mechanical Keyboard', 20, '2024-01-14'),
('c792da12-1022-4b9a-b00e-73d3e9dd3134', 'Qwertykeys', '2024-01-14', true, 'QK65', 'Group Buy', 'Mechanical Keyboard', 30, '2024-01-14'),
('c792da12-1022-4b9a-b00e-73d3e9dd3135', 'Neo', '2024-01-14', true, 'neo65', 'Group Buy', 'Mechanical Keyboard', 40, '2024-01-14'),
('c792da12-1022-4b9a-b00e-73d3e9dd3136', 'Neo', '2024-01-14', true, 'neo70', 'Group Buy', 'Mechanical Keyboard', 50, '2024-01-14'),
('c792da12-1022-4b9a-b00e-73d3e9dd3137', 'Neo', '2024-01-14', true, 'neo80', 'Group Buy', 'Mechanical Keyboard', 60, '2024-01-14'),
('c792da12-1022-4b9a-b00e-73d3e9dd3138', 'Owlab', '2024-01-14', true, 'Neon', 'In Stock', 'Keyboard Switch', 70, '2024-01-14'),
('c792da12-1022-4b9a-b00e-73d3e9dd3139', 'Vertex', '2024-01-14', true, 'V One', 'In Stock', 'Keyboard Switch', 80, '2024-01-14'),
('c792da12-1022-4b9a-b00e-73d3e9dd3140', 'GMK', '2024-01-14', true, 'Firefly', 'Pre - Order', 'Keycaps', 90, '2024-01-14');
